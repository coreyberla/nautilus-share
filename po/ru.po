# Russian translations for nautilus-share package.
# Copyright (C) 2005 Free Software Foundation
# This file is distributed under the same license as the nautilus-share package.
# Stas Solovey <whats_up@tut.by>, 2012, 2013, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: nautilus-share trunk\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=nautilus-share&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2013-12-19 15:53+0000\n"
"PO-Revision-Date: 2015-11-10 19:27+0300\n"
"Last-Translator: Stas Solovey <whats_up@tut.by>\n"
"Language-Team: Russian <gnome-cyr@gnome.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../src/nautilus-share.c:123
#, c-format
msgid ""
"Nautilus needs to add some permissions to your folder \"%s\" in order to "
"share it"
msgstr ""
"Для общего доступа к папке «%s» необходимо добавить дополнительные права "
"доступа"

# пробела между %s%s%s и "добавить" не должно быть, в оригинале все верно
#: ../src/nautilus-share.c:131
#, c-format
msgid ""
"The folder \"%s\" needs the following extra permissions for sharing to "
"work:\n"
"%s%s%sDo you want Nautilus to add these permissions to the folder "
"automatically?"
msgstr ""
"Для общего доступа к папке «%s» необходимо добавить следующие дополнительные "
"права доступа:\n"
"%s%s%sДобавить права доступа автоматически?"

#: ../src/nautilus-share.c:135
msgid "  - read permission by others\n"
msgstr "  — чтение для всех\n"

#: ../src/nautilus-share.c:136
msgid "  - write permission by others\n"
msgstr "  — запись для всех\n"

#: ../src/nautilus-share.c:137
msgid "  - execute permission by others\n"
msgstr "  — выполнение для всех\n"

#: ../src/nautilus-share.c:141
msgid "Add the permissions automatically"
msgstr "Установить права автоматически"

#: ../src/nautilus-share.c:167
#, c-format
msgid "Could not change the permissions of folder \"%s\""
msgstr "Не удалось изменить права доступа к папке «%s»"

#: ../src/nautilus-share.c:446
msgid "Share name is too long"
msgstr "Имя ресурса слишком длинное"

#: ../src/nautilus-share.c:480
msgid "The share name cannot be empty"
msgstr "Введите имя ресурса"

#: ../src/nautilus-share.c:493
#, c-format
msgid "Error while getting share information: %s"
msgstr "Ошибка получения сведений о ресурсе: %s"

#: ../src/nautilus-share.c:503
msgid "Another share has the same name"
msgstr "Ресурс с таким именем уже существует"

#: ../src/nautilus-share.c:550 ../src/nautilus-share.c:786
msgid "Modify _Share"
msgstr "Из_менить ресурс"

#: ../src/nautilus-share.c:550 ../src/nautilus-share.c:788
msgid "Create _Share"
msgstr "_Создать ресурс"

#: ../src/nautilus-share.c:678
msgid "There was an error while getting the sharing information"
msgstr "При получении сведений о ресурсе произошла ошибка"

#: ../src/nautilus-share.c:1050
msgid "Share"
msgstr "Общий доступ"

#: ../src/nautilus-share.c:1110
msgid "Folder Sharing"
msgstr "Общий доступ к папке"

#: ../src/nautilus-share.c:1151
msgid "Sharing Options"
msgstr "Настроить общий доступ"

#: ../src/nautilus-share.c:1152
msgid "Share this Folder"
msgstr "Открыть общий доступ к папке"

#: ../src/shares.c:125
#, c-format
msgid "%s %s %s returned with signal %d"
msgstr "Процесс %s %s %s завершился с сигналом %d"

#: ../src/shares.c:134
#, c-format
msgid "%s %s %s failed for an unknown reason"
msgstr "Неизвестная ошибка при запуске %s %s %s"

#: ../src/shares.c:154
#, c-format
msgid "'net usershare' returned error %d: %s"
msgstr "Ошибка %d при запуске «net usershare»: %s"

#: ../src/shares.c:156
#, c-format
msgid "'net usershare' returned error %d"
msgstr "Ошибка %d при запуске «net usershare»"

#: ../src/shares.c:187
#, c-format
msgid "the output of 'net usershare' is not in valid UTF-8 encoding"
msgstr "вывод «net usershare» не является корректной строкой UTF-8"

#: ../src/shares.c:442 ../src/shares.c:616
#, c-format
msgid "Failed"
msgstr "Ошибка"

#: ../src/shares.c:550
#, c-format
msgid "Samba's testparm returned with signal %d"
msgstr "Процесс testparm завершился с сигналом %d"

#: ../src/shares.c:556
#, c-format
msgid "Samba's testparm failed for an unknown reason"
msgstr "Неизвестная ошибка при запуске testparm"

#: ../src/shares.c:571
#, c-format
msgid "Samba's testparm returned error %d: %s"
msgstr "Ошибка %d при запуске testparm: %s"

#: ../src/shares.c:573
#, c-format
msgid "Samba's testparm returned error %d"
msgstr "Ошибка %d при запуске testparm"

#: ../src/shares.c:684
#, c-format
msgid "Cannot remove the share for path %s: that path is not shared"
msgstr "Не удалось убрать общий доступ к папке %s: эта папка не опубликована"

#: ../src/shares.c:730
#, c-format
msgid ""
"Cannot change the path of an existing share; please remove the old share "
"first and add a new one"
msgstr ""
"Не удалось изменить путь существующего ресурса; удалите старый ресурс и "
"добавьте новый"

#: ../interfaces/share-dialog.ui.h:1
msgid "<big><b>Folder Sharing</b></big>"
msgstr "<big><b>Общий доступ к папке</b></big>"

#: ../interfaces/share-dialog.ui.h:2
msgid "Share _name:"
msgstr "_Имя ресурса:"

#: ../interfaces/share-dialog.ui.h:3
msgid "Share this _folder"
msgstr "_Открыть общий доступ к папке"

#: ../interfaces/share-dialog.ui.h:4
msgid "Co_mment:"
msgstr "_Комментарий:"

#: ../interfaces/share-dialog.ui.h:5
msgid "_Allow others to create and delete files in this folder"
msgstr "_Разрешить создание и удаление файлов по сети"

#: ../interfaces/share-dialog.ui.h:6
msgid "_Guest access (for people without a user account)"
msgstr "Разрешить _доступ без локальной учётной записи"
